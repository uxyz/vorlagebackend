import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {MessageDTO} from '../messageDTO';
import {Message} from '../message';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../security.service';
import {ActivatedRoute, Router, Routes} from '@angular/router';



@Component({
  selector: 'app-inoutbox',
  templateUrl: './inoutbox.component.html',
  styleUrls: ['./inoutbox.component.css']

})
export class InoutboxComponent implements OnInit {


  sessionUser: User | null = null;
  postedAt: Date = new Date();
  flashMessage = {message: ''};
  messageData: MessageDTO = {
    text: '',
  };


  private id: number;
  private sub: any;
  private idString: string;
  private idStringY: string;
  private idStringE: string;
  private uid: number;
  username: string;
  messages: Message[] = [];
  message: Message;
  isChecked: boolean;
  iChecked: boolean;
  checkarray: any = [];

  constructor(private http: HttpClient, private securityService: SecurityService, private router: Router,
              private route: ActivatedRoute) {

    this.isChecked = false;
  }












  checkboxes: any [] = [
    {messageid: 'this.uid', checked: false },


  ];




  isCheckedd(item: any): boolean {

    if (this.checkarray.find(x => x.id === item.id)) {

      return true;
    }else{
      return false;
    }
  }






  checked(){
    if (this.isChecked === true)
    {
      this.isChecked = false;
    }
    else
    {
      this.isChecked = true;
    }

  }

  CheckAllOptions() {
    if (this.checkboxes.every(val => val.checked === true)) {
      this.checkboxes.forEach(val => { val.checked = false; });
    }
    else {
      this.checkboxes.forEach(val => { val.checked = true; });
    }
    console.log(this.checkboxes);
  }
  loeschen(message) {
    const  element =  (document.getElementsByName('umut') as any  as HTMLInputElement[]);

    for (const elm of element) {


      if (elm.checked){

        const messageid = elm.value.toString();
        this.http.post<Message>('/api/checkboxout/' + messageid, null)
          .subscribe(mess =>
            message = mess


          );
        this.router.navigateByUrl('');
      }
      console.log(elm.value);
      console.log(elm.checked);
    }
  }

  deletechecked(message){

        for (const messagex of this.messages) {
                messagex.checkboxout = true;


                const messageid = messagex.id.toString();

                this.http.post<Message>('/api/checkboxout/' + messageid, null)
            .subscribe(mess =>
                message = mess


            );
                this.router.navigateByUrl('');
     /*     console.log(message.id);
          console.log(message.deletedin);
          console.log(message);*/
        }
      }




  ngOnInit(): void {


    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
      });

    // this.username = this.sessionUser.username;
    // this.idString = this.sessionUser.id.toString();

    this.sub = this.route.params.subscribe(params => {
      this.id = +params.id;

      this.idString = this.id.toString();
    });

    this.http.get<Message[]>('/api/message/' + this.idString)

      .subscribe(messages => {

        this.messages = messages;

        for (const message of this.messages) {
          this.uid = message.id;
          console.log(message.userName);
          console.log(this.uid);
          this.checkboxes.forEach(val => { val.messageid = this.uid; });
          console.log(this.checkboxes);
          console.log(message.checkboxout);

        }

      });

  }



  deletemessage(message) {

    const messageid = message.id.toString();
    message.deletedout = true;
    console.log(message.deletedout.toString());
    this.http.post<Message>('/api/messagedeleteo/' + messageid, null)
      .subscribe(mess =>
          message = mess
        // this.router.navigateByUrl('');

      );
    console.log(message.id);
    console.log(message.deletedin);
    console.log(message);


  }
}
