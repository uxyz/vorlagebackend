import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {RouterModule, Routes} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';

import {ModalModule} from 'ngx-bootstrap/modal';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import { UeberschriftSchreibenComponent } from './ueberschrift-schreiben/ueberschrift-schreiben.component';
import { EintragSchreibenComponent } from './eintrag-schreiben/eintrag-schreiben.component';
import { UeberschriftDetailsComponent } from './ueberschrift-details/ueberschrift-details.component';
import { LeftFrameComponent } from './left-frame/left-frame.component';
import {LoginComponent} from './login/login.component';
import {SessionUserComponent} from './session-user/session-user.component';
import {TopComponent} from './top/top.component';
import {FooterComponent} from './footer/footer.component';
import {RegistrationComponent} from './registration/registration.component';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { UserLikeComponent } from './user-like/user-like.component';
import { HomeComponent } from './home/home.component';
import { NextpageComponent } from './nextpage/nextpage.component';
import { MessageComponent } from './message/message.component';
import { InoutboxComponent } from './inoutbox/inoutbox.component';
import { MessageBoxComponent } from './message-box/message-box.component';
import { MessageLargeComponent } from './message-large/message-large.component';




const routes: Routes = [
   // { path: '', pathMatch: 'full', redirectTo: ''},
  {path: 'ueberschrift-schreiben', component: UeberschriftSchreibenComponent},
  {path: 'ueberschrift-details', component: UeberschriftDetailsComponent},
   {path: 'eintrag-schreiben', component: EintragSchreibenComponent},
  {path: 'eintrag-schreiben/:id', component: EintragSchreibenComponent},
  {path: 'ueberschriftDetail/:id', component: UeberschriftDetailsComponent},

  {path: 'eintrag/:id', component: EintragSchreibenComponent},
  // {path: 'eintrag/:id/:uid', component: EintragSchreibenComponent},

  // {path: 'eintrag-schreiben/:id/:uid', component: EintragSchreibenComponent},
  {path: '', component: HomeComponent},
  {path: 'user-like/:id', component: UserLikeComponent},
  {path: 'ueberschrifts/:id', component: NextpageComponent},
  {path: 'message/:id/:uid', component: MessageComponent},
   {path: 'message/:id', component: MessageBoxComponent}



];


@NgModule({
  declarations: [
    AppComponent,
    UeberschriftSchreibenComponent,
    EintragSchreibenComponent,
    UeberschriftDetailsComponent,
    LeftFrameComponent,
    LoginComponent,
    RegistrationComponent,
    SessionUserComponent,
    TopComponent,
    FooterComponent,
    SearchbarComponent,
    UserLikeComponent,
    HomeComponent,
    NextpageComponent,
    MessageComponent,
    InoutboxComponent,
    MessageBoxComponent,
    MessageLargeComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes),
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    ReactiveFormsModule,
    NgbTypeaheadModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
