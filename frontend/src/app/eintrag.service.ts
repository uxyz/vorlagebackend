import {Eintrag} from './eintrag';
import {BehaviorSubject} from 'rxjs';
import {Injectable} from '@angular/core';



@Injectable({
  providedIn: 'root'
})


export class EintragService {

  private eintrag: Eintrag;
  private eintragSource = new BehaviorSubject<Eintrag>(this.eintrag);
  currentEintrag = this.eintragSource.asObservable();
  constructor() {
  }


  setEintrag(eintrag: Eintrag){
    this.eintragSource.next(eintrag);
  }

}
