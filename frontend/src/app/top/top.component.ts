import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Ueberschrift} from '../ueberschrift';
import {Message} from '../message';

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.css']
})
export class TopComponent implements OnInit{
  title = 'frontend';

  sessionUser: User|null = null;
  ueberschriftList: Ueberschrift[];
  messageList: Message [] = [];
  unreadedmessages: Message [] = [];


  constructor(private securityService: SecurityService, private http: HttpClient,

              private router: Router) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
        if (u !== null){
          const userid = this.sessionUser.id.toString();

          this.http.get<Message []>('/api/message/' + userid)
            .subscribe(messages => {this.messageList = messages;

                                    for ( const message of this.messageList){
            if (!message.readmessage && message.userNameA === this.sessionUser.username){
              this.unreadedmessages.push(message);
            }
            console.log(this.messageList.length);
          }
        });
          console.log(this.messageList);
      }});
    console.log(this.sessionUser);
    this.http.get<Ueberschrift[]>('/api/ueberschrifts')
      .subscribe(ueberschriftList => {
        this.ueberschriftList = ueberschriftList;
      });


  }

  logout() {
    this.securityService.logout();
    this.router.navigate(['/']);
  }


}
