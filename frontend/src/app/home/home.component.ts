import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {Ueberschrift} from '../ueberschrift';
import {Eintrag} from '../eintrag';
import {UserLike} from '../userLike';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../security.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  sessionUser: User | null = null;

  private id: number;
  private sub: any;
  private idString: string;
  ueberschrift: Ueberschrift;
  eintragList: Eintrag[] = [];
  eintrag: Eintrag;
  likesnumber: number[] = [];
  likeseintrags: number [] = [];
  likesuserid: number[] = [];
  flashMessage = {message: ''};
  eintragidstring: string;
  likeUserE: UserLike[] = [];
  ueberschriftList: Ueberschrift [] = [];
  zahl: string;
  eintragList1: Eintrag[] = [];


  constructor(private http: HttpClient, private securityService: SecurityService, private router: Router,
              private route: ActivatedRoute) {
  }


  deleteEitrag(eintrag) {
    const userid = this.sessionUser.id.toString();
    this.eintragidstring = eintrag.id.toString();
    this.idString = eintrag.ueberschrift.id.toString();
    this.http.post<Eintrag[]>('/api/deleteeintrag/' + this.eintragidstring + '/' + userid + '/' + this.idString, null)
      .subscribe(eintr => this.eintragList = eintr );
    this.router.navigateByUrl('/ueberschriftDetail/' + this.idString).then();


    // this.http.post<UserLike[]>('/api/deletelikes/' + this.eintragidstring + '/' + userid, null);

    console.log(userid);
    console.log(this.eintragidstring);


  }


  addToLikes(eintrag) {

    const userid = this.sessionUser.id.toString();
    this.eintragidstring = eintrag.id.toString();
    console.log(this.eintragidstring);
    this.http.post<UserLike[]>('/api/likesnumber/' + this.eintragidstring + '/' + userid, null)
      .subscribe(eintraglike => {
        this.likeUserE = eintraglike;

        for (const likeEintrag of eintraglike) {
          this.likeseintrags.push(likeEintrag.eintrag.id);
        }
      });

  }

  removelike(eintrag) {
    this.likeUserE.splice(eintrag);
    const userid = this.sessionUser.id.toString();
    this.eintragidstring = eintrag.id.toString();
    this.http.post<UserLike[]>('/api/unlikesnumber/' + this.eintragidstring + '/' + userid, null)
      .subscribe(eintraglike => {
        this.likeUserE = eintraglike;
      });
  }

  /*nextpage(){
    this.http.get <Ueberschrift[]>('/api/ueberschrift/')
      .subscribe(ueberschrift => {
        this.ueberschriftList = ueberschrift.slice(4, 8);
        console.log(this.ueberschriftList);
        for (const uebr of this.ueberschriftList) {
          this.zahl = uebr.id.toString();
          console.log(uebr.eintragList);
          this.http.get<Eintrag[]>('/api/eintragss/' + this.zahl)
            .subscribe(eintragList => {
              /!*          if (eintragList.length > 2){
                          this.eintragList = eintragList.slice(0, 2);
                        }else {*!/
              this.eintragList = eintragList.slice(0, 2);


              console.log(this.eintragList);

              for (const eintr of this.eintragList) {
                if (eintr.ueberschrift.id === uebr.id) {
                  uebr.eintragList.push(eintr);
                  console.log(uebr.eintragList);
                }}

            });

        }});

  }*/


  ngOnInit(): void {

    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
        if (u != null) {
          const idUser = this.sessionUser.id.toString();
          this.http.get<UserLike[]>('/api/userlikes/' + idUser)
            .subscribe(userlikes => {
              this.likeUserE = userlikes;
              for (const usrlik of userlikes) {
                this.likeseintrags.push(usrlik.eintrag.id);
                if (!this.likesnumber.includes(usrlik.eintrag.id)) {
                  this.likesnumber.push(usrlik.eintrag.id);
                }
                if (!this.likesuserid.includes(this.sessionUser.id)) {
                  this.likesuserid.push(this.sessionUser.id);
                }
              }
            });

        }


      });
    this.http.get <Ueberschrift[]>('/api/ueberschrift/')
      .subscribe(ueberschrift => {
        this.ueberschriftList = ueberschrift.slice(0, 4);
        console.log(this.ueberschriftList);
        for (const uebr of this.ueberschriftList) {
          this.zahl = uebr.id.toString();
          console.log(uebr.eintragList);
          this.http.get<Eintrag[]>('/api/eintragss/' + this.zahl)
            .subscribe(eintragList => {
              /*          if (eintragList.length > 2){
                          this.eintragList = eintragList.slice(0, 2);
                        }else {*/
              this.eintragList = eintragList.slice(0, 2);


              console.log(this.eintragList);

              for (const eintr of this.eintragList) {
                if (eintr.ueberschrift.id === uebr.id) {
                  uebr.eintragList.push(eintr);
                  console.log(uebr.eintragList);
                }}

            });

        }});






/*
      for (const eintr of this.eintragList) {
        if (eintr.ueberschrift.id === uebr.id) {
          uebr.eintragList.push(eintr);
        }
        console.log(uebr.eintragList);

      */

    /*        for (const ubr of this.ueberschriftList){
                console.log(ubr);
              }*/




    this.http.get<Eintrag[]>('/api/eintrag/')
        .subscribe(
          eintragList1 => {

            this.eintragList1 = eintragList1;

            for (const eint of this.eintragList1) {

              console.log(eint);
              console.log(eint.userid);
              // console.log(this.sessionUser.id);
              console.log(eint.username);
              console.log(this.eintragList1);

              // console.log(this.sessionUser.username);
            }
          });

      /*      setFlashMessage(message: string) {
          this.flashMessage.message = message;
        }*/
    }
  }

