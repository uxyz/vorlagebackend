import {User} from './user';


export interface Message {
  id: number;
  text: string;
  postedAt: Date;
  ldt: Date;
  realTime: string;
  user: User;
  user_id: number;
  userid: number;
  userName: string;
  userNameA: string;
  userList: User[];
  deletedin: boolean;
  deletedout: boolean;
  checkboxin: boolean;
  checkboxout: boolean;
  readmessage: boolean;

}
