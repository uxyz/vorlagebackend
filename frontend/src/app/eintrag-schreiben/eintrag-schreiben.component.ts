import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {EintragDTO} from '../eintragDTO';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {SecurityService} from '../security.service';
import {Eintrag} from '../eintrag';
import {Ueberschrift} from '../ueberschrift';


@Component({
  selector: 'app-eintrag-schreiben',
  templateUrl: './eintrag-schreiben.component.html',
  styleUrls: ['./eintrag-schreiben.component.css']
})
export class EintragSchreibenComponent implements OnInit {


  sessionUser: User | null = null;
  private id: number;
  private sub: any;
  private idString: string;
  private idStringY: string;
  private idStringE: string;
  private uid: number;
  private eintrag: Eintrag;
  username: string;
  private eintragid: number;
  eintragData: EintragDTO = {
    text: '',
  };
  postedAt: Date = new Date();
  flashMessage = {message: ''};
  ueberschrift: Ueberschrift;

  constructor(private http: HttpClient, private securityService: SecurityService, private router: Router,
              private route: ActivatedRoute) {
  }




  ngOnInit(): void {



    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
      });
    this.sub = this.route.params.subscribe(params => {
      this.id = +params.id;

      this.idString = this.id.toString();
    });

    this.sub = this.route.params.subscribe(params => {
      this.uid = +params.uid;

      this.idStringY = this.uid.toString();


    });
    this.http.get <Ueberschrift>('/api/ueberschrift/' + this.idString)
      .subscribe(ueberschrift => {
        this.ueberschrift = ueberschrift; });


    this.sub = this.route.params.subscribe(params => {
      this.eintragid = +params.eintragid;

      this.idStringE = this.eintragid.toString();


    });


  }

  refresh(): void {
    window.location.reload();
  }

  eintragschreiben() {
    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
      });

    this.username = this.sessionUser.username;

    if (this.eintragData.text.length < 1) {
      this.setFlashMessage('you must enter something');
    } else {
      this.http.post<Eintrag>('/api/eintrag-schreiben/' + this.idString + '/' + this.idStringY + '/' + this.idStringE, {
        text: this.eintragData.text,
        postedAt: this.postedAt,
        user: this.sessionUser,
        username: this.username,
        id: this.eintragid,
        userid: this.sessionUser.id,
      })
        .subscribe(
          eintrag => {
            this.eintrag = eintrag;
            if (eintrag != null) {
              this.setFlashMessage(null);
              this.router.navigateByUrl('/ueberschriftDetail/' + this.idString).then();

/*              this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
                this.router.navigate(['/eintrag-schreiben/' + this.idString + '/' + this.idStringY + '/' + this.idStringE]); });*/
            } else {
              this.setFlashMessage('eintrag==null');
            }

          });


      this.http.get<User>('/api/user/' + this.idStringY)
        .subscribe(u => {
          this.sessionUser = u;
          this.sessionUser.username = this.username;
        });
    }


  }

  setFlashMessage(message: string) {
    this.flashMessage.message = message;
  }

}
