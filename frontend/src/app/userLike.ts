import {User} from './user';
import {Eintrag} from './eintrag';


export interface UserLike {
  id: number;
  user: User;
  eintrag: Eintrag;
  eintragid: number;

}
