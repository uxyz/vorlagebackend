import {Injectable} from '@angular/core';
import {Ueberschrift} from './ueberschrift';
import {BehaviorSubject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})


export class UeberschriftService {
  private ueberschrift: Ueberschrift;
  private ueberschriftSource = new BehaviorSubject<Ueberschrift>(this.ueberschrift);
  currentUeberschrift = this.ueberschriftSource.asObservable();

  constructor() {
  }

  setUeberschrift(ueberschrift: Ueberschrift){
    this.ueberschriftSource.next(ueberschrift);
  }

}
