import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../security.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../user';
import {Ueberschrift} from '../ueberschrift';
import {Eintrag} from '../eintrag';
import {UserLike} from '../userLike';

@Component({
  selector: 'app-nextpage',
  templateUrl: './nextpage.component.html',
  styleUrls: ['./nextpage.component.css']
})
export class NextpageComponent implements OnInit {

  sessionUser: User | null = null;

  private id: number;
  private sub: any;
  private idString: string;
  ueberschrift: Ueberschrift;
  eintragList: Eintrag[] = [];
  eintrag: Eintrag;
  likesnumber: number[] = [];
  likeseintrags: number [] = [];
  likesuserid: number[] = [];
  flashMessage = {message: ''};
  eintragidstring: string;
  likeUserE: UserLike[] = [];
  ueberschriftList: Ueberschrift [] = [];
  zahl: string;
  eintragList1: Eintrag[] = [];

  constructor(private http: HttpClient, private securityService: SecurityService, private router: Router,
              private route: ActivatedRoute) { }


  deleteEitrag(eintrag) {
    const userid = this.sessionUser.id.toString();
    this.eintragidstring = eintrag.id.toString();
    this.idString = eintrag.ueberschrift.id.toString();
    this.http.post<Eintrag[]>('/api/deleteeintrag/' + this.eintragidstring + '/' + userid + '/' + this.idString, null)
      .subscribe(eintr => this.eintragList = eintr );

    // this.http.post<UserLike[]>('/api/deletelikes/' + this.eintragidstring + '/' + userid, null);

    console.log(userid);
    console.log(this.eintragidstring);


  }


  addToLikes(eintrag) {

    const userid = this.sessionUser.id.toString();
    this.eintragidstring = eintrag.id.toString();
    console.log(this.eintragidstring);
    this.http.post<UserLike[]>('/api/likesnumber/' + this.eintragidstring + '/' + userid, null)
      .subscribe(eintraglike => {
        this.likeUserE = eintraglike;

        for (const likeEintrag of eintraglike) {
          this.likeseintrags.push(likeEintrag.eintrag.id);
        }
      });

  }

  removelike(eintrag) {
    this.likeUserE.splice(eintrag);
    const userid = this.sessionUser.id.toString();
    this.eintragidstring = eintrag.id.toString();
    this.http.post<UserLike[]>('/api/unlikesnumber/' + this.eintragidstring + '/' + userid, null)
      .subscribe(eintraglike => {
        this.likeUserE = eintraglike;
      });
  }

  ngOnInit(): void {



    this.sub = this.route.params.subscribe(params => {
      this.id = +params.id;
    });

    this.http.get <Ueberschrift[]>('/api/ueberschrift/')
      .subscribe(ueberschrift => {
        this.ueberschriftList = ueberschrift.slice(this.id * 4 - 4, this.id * 4);
        console.log(this.ueberschriftList);
        for (const uebr of this.ueberschriftList) {
          this.zahl = uebr.id.toString();
          console.log(uebr.eintragList);
          this.http.get<Eintrag[]>('/api/eintragss/' + this.zahl)
            .subscribe(eintragList => {
              /*if (eintragList.length > 2){
              this.eintragList = eintragList.slice(0, 2);
            }else {*/
          this.eintragList = eintragList.slice(0, 2);


          console.log(this.eintragList);

          for (const eintr of this.eintragList) {
              if (eintr.ueberschrift.id === uebr.id) {
                uebr.eintragList.push(eintr);
                console.log(uebr.eintragList);
              }}

          });

        }});
  }

}
