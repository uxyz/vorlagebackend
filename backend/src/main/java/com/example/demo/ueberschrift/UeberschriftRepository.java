package com.example.demo.ueberschrift;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface UeberschriftRepository extends CrudRepository<Ueberschrift, Long> {

    Set<Ueberschrift> findAll();


    Ueberschrift findById(long id);

    Ueberschrift findByText(String text);

    Set<Ueberschrift> findByUserId(long id);



}
