package com.example.demo.user;

import com.example.demo.eintrag.Eintrag;
import com.example.demo.message.Message;
import com.example.demo.ueberschrift.Ueberschrift;


import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String username;
    private String password;
    private boolean admin;

    @OneToMany(mappedBy = "user")
    private Set<Ueberschrift> ueberschriftsList;

    @ManyToMany(mappedBy = "user")
    private Set<Message> messageList;

    @OneToMany(mappedBy = "user")
    private Set<Eintrag> eintragList;

    public User() {
    }

    public User(String username) {
        this.username = username;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.admin = false;
    }


    public User(String username, long id) {

        this.username = username;
        this.id = id;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }




    public User(long id) {
        this.id = id;
    }

    public Set<Ueberschrift> getUeberschriftsList() {
        return ueberschriftsList;
    }

    public void setUeberschriftsList(Set<Ueberschrift> ueberschriftsList) {
        this.ueberschriftsList = ueberschriftsList;
    }

    public Set<Eintrag> getEintragList() {
        return eintragList;
    }

    public void setEintragList(Set<Eintrag> eintragList) {
        this.eintragList = eintragList;
    }

    public Set<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(Set<Message> messageList) {
        this.messageList = messageList;
    }
}
