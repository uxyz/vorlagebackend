package com.example.demo.UserLike;


import com.example.demo.eintrag.EintragService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
public class UserLikeController {

    private UserLikeService userLikeService;
    private EintragService eintragService;

    @Autowired
    public UserLikeController(UserLikeService userLikeService, EintragService eintragService) {
        this.userLikeService = userLikeService;
        this.eintragService = eintragService;
    }

    @PostMapping("/api/likesnumber/{eintragid}/{userid}")
    public Set<UserLike> addToLikes(@PathVariable("eintragid") long id, @PathVariable ("userid") long userid){
        this.eintragService.getEintragRepository().findEintragById(id).setLikesnumber(this.eintragService.getEintragRepository().findEintragById(id).getLikesnumber()+1);
        return userLikeService.addToLikes(userid,id);
    }

    @GetMapping("/api/userlikes/{userid}")
    public Set<UserLike> showlikesById(@PathVariable("userid") long userid){
        return userLikeService.getLikesByUserId(userid);
    }

    @PostMapping("/api/unlikesnumber/{eintragid}/{userid}")
    public Set<UserLike> removelike(@PathVariable("eintragid") long id, @PathVariable ("userid") long userid){
        this.eintragService.getEintragRepository().findEintragById(id).setLikesnumber(this.eintragService.getEintragRepository().findEintragById(id).getLikesnumber()-1);
        return userLikeService.removelikes(userid,id);
    }
}
