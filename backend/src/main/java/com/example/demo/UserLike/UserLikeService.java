package com.example.demo.UserLike;


import com.example.demo.eintrag.Eintrag;
import com.example.demo.eintrag.EintragService;
import com.example.demo.user.User;
import com.example.demo.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class UserLikeService {

    private UserLikeRepository userLikeRepository;
    private UserService userService;
    private EintragService eintragService;

    @Autowired
    public UserLikeService(UserLikeRepository userLikeRepository,
                           UserService userService, EintragService eintragService){

        this.userLikeRepository=userLikeRepository;
        this.userService =userService;
        this.eintragService = eintragService;

    }


    public Set<UserLike>getLikesByUserId(long id){
        return userLikeRepository.findAllByUserId(id);
    }

    public Set<UserLike>getLikesByEintragId(long id){
        return userLikeRepository.findByEintragId(id);
    }
    public Set<UserLike> addToLikes(long userid, long id) {

        if(userLikeRepository.findByUserIdAndEintragId(userid,id) == null){
            User user =userService.getUserById(userid);
            Eintrag eintrag = eintragService.getEintragRepository().findEintragById(id);
            userLikeRepository.save(new UserLike(user,eintrag));
        }
        return getLikesByUserId(userid);


        /*this.eintragRepository.findEintragById(id).setLikesnumber(this.eintragRepository.findEintragById(id).getLikesnumber()+1);*/

    }

    public Set<UserLike> removelikes(long userid, long id){
        UserLike userLike = userLikeRepository.findByUserIdAndEintragId(userid,id);
        if(userLike != null){
            userLikeRepository.delete(userLike);
        }
        return getLikesByUserId(userid);
    }
}
