package com.example.demo.UserLike;


import com.example.demo.eintrag.Eintrag;
import com.example.demo.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class UserLike {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @ManyToOne
    @JoinColumn(name="user_id")
    @JsonIgnore
    private User user;

    @ManyToOne
    @JoinColumn(name = "eintrag_id")
    private Eintrag eintrag;


    public UserLike() {
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Eintrag getEintrag() {
        return eintrag;
    }

    public void setEintrag(Eintrag eintrag) {
        this.eintrag = eintrag;
    }

    public UserLike(User user, Eintrag eintrag) {
        this.user = user;
        this.eintrag = eintrag;
    }
}

