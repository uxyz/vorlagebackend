package com.example.demo.eintrag;

import com.example.demo.ueberschrift.Ueberschrift;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface EintragRepository extends CrudRepository<Eintrag, Long> {

    List<Eintrag> findAll();
    List<Eintrag> findEintragByUeberschriftId(long id);
    List<Eintrag> findByUserid(long userid);

    // (List<Eintrag> findEintragByUeberschrift(Ueberschrift ueberschrift);

    Eintrag findEintragById(long id);
    Eintrag findEintragByText(String text);
    Eintrag findByUserIdAndId(long userid, long eintragid);
}
