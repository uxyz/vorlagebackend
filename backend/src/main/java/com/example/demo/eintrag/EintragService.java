package com.example.demo.eintrag;


import com.example.demo.UserLike.UserLike;
import com.example.demo.UserLike.UserLikeRepository;
import com.example.demo.ueberschrift.Ueberschrift;
import com.example.demo.ueberschrift.UeberschriftRepository;
import com.example.demo.user.User;
import com.example.demo.user.UserRepository;
import com.example.demo.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Set;

@Service
public class EintragService {

    private EintragRepository eintragRepository;
    private UeberschriftRepository ueberschriftRepository;
    private UserService userService;
    private UserRepository userRepository;
    private UserLikeRepository userLikeRepository;

    @Autowired
    public EintragService(EintragRepository eintragRepository, UserLikeRepository userLikeRepository, UeberschriftRepository ueberschriftRepository, UserService userService, UserRepository userRepository) {
        this.eintragRepository = eintragRepository;
        this.ueberschriftRepository = ueberschriftRepository;
        this.userService = userService;
        this.userRepository = userRepository;
        this.userLikeRepository = userLikeRepository;

    }

    public EintragRepository getEintragRepository() {
        return eintragRepository;
    }

    public Eintrag eintragSchreiben(String text, long id, Instant postedAt, long uid, String username, long eintragid) {
        Ueberschrift ueberschrift = ueberschriftRepository.findById(id);
        //  ueberschrift.setEintragList(eintragRepository.findEintragByUeberschriftId(id));
        User user = userRepository.findUserById(uid);
        // user.setUsername(username);
        // ueberschrift.getEintragList().add(eintragRepository.findEintragById(eintragid));

        return eintragRepository.save(new Eintrag(user, ueberschrift, text, postedAt, username));

    }

    public Set<UserLike> getUserLikesByUserId(long id) {
        return userLikeRepository.findAllByUserId(id);
    }


    public List<Eintrag> deleteEintrag(long userid, long eintragid) {

        Eintrag eintrag = this.getEintragRepository().findByUserIdAndId(userid, eintragid);
        Set<UserLike> userLike = this.userLikeRepository.findByEintragId(eintragid);
        for(UserLike i: userLike){
            this.userLikeRepository.delete(i);

        }

        if (eintrag != null) {

            this.getEintragRepository().delete(eintrag);

        }




        return this.eintragRepository.findAll();

    }
}



