package com.example.demo.eintrag;


import com.example.demo.UserLike.UserLike;
import com.example.demo.UserLike.UserLikeService;
import com.example.demo.ueberschrift.UeberschriftService;
import com.example.demo.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;


@RestController
public class EintragController {

    private EintragService eintragService;
    private UeberschriftService ueberschriftService;
    private UserService userService;
    private UserLikeService userLikeService;

    @Autowired
    public EintragController(EintragService eintragService, UserService userService,UserLikeService userLikeService, UeberschriftService ueberschriftService){
        this.eintragService=eintragService;
        this.userService=userService;
        this.userLikeService = userLikeService;
        this.ueberschriftService = ueberschriftService;
    }

    @GetMapping("/api/eintrag/{ueberschriftId}")
    public List<Eintrag> showAllEintrags(@PathVariable("ueberschriftId") long id){
        return eintragService.getEintragRepository().findEintragByUeberschriftId(id);
    }


    @GetMapping("/api/eintragss/{ueberschriftid}")
    public List<Eintrag> showAllEintrag(@PathVariable("ueberschriftid") long id){
        return eintragService.getEintragRepository().findEintragByUeberschriftId(id);}


@GetMapping("/api/eintrag/")
public List<Eintrag> showAllEintragee(){
        return eintragService.getEintragRepository().findAll();
}


    @PostMapping("/api/eintrag-schreiben/{ueberschriftId}/{userId}/{eintragid}")
    public Eintrag eintragSchreiben(@RequestBody Eintrag eintrag, @PathVariable("ueberschriftId") long id, @PathVariable("userId") long uid,
                                    @PathVariable("eintragid") long eintragid){

        this.ueberschriftService.getUeberschriftById(id).setEintragListLenght(this.ueberschriftService.getUeberschriftById(id).getEintragListLenght()+1);
        return eintragService.eintragSchreiben(eintrag.getText(),id,eintrag.getPostedAt(),uid,eintrag.getUsername(),eintragid);
    }

    @GetMapping("/api/eintrags/{userid}")
    public Set<UserLike> showuserlikes(@PathVariable("userid") long userid){
        return userLikeService.getLikesByUserId(userid);
    }




    // profil sayfasi icin bu

    @GetMapping("/api/eintragS/{userid}")
    public List<Eintrag> showeintragS(@PathVariable("userid") long userid){
        return eintragService.getEintragRepository().findByUserid(userid);
    }






    @PostMapping("/api/deleteeintrag/{eintragid}/{userid}/{uebershriftid}")
    public List<Eintrag> deleteEintrag(@PathVariable("eintragid") long eintragid,@PathVariable("userid") long userid,
                                       @PathVariable ("uebershriftid") long uebershriftid){
        this.ueberschriftService.getUeberschriftById(uebershriftid).setEintragListLenght(this.ueberschriftService.getUeberschriftById(uebershriftid).getEintragListLenght()-1);

        return this.eintragService.deleteEintrag(userid,eintragid);
    } // bu cok önemli, List<Eintrag> bunu döndüren eintragservice deki deleteEintragmetodu


    @PostMapping("/api/deleteeintragd/{eintragid}/{userid}/{uebershriftid}")
    public List<Eintrag> deleteEintragd(@PathVariable("eintragid") long eintragid,@PathVariable("userid") long userid,
                                       @PathVariable ("uebershriftid") long uebershriftid){
        this.ueberschriftService.getUeberschriftById(uebershriftid).setEintragListLenght(this.ueberschriftService.getUeberschriftById(uebershriftid).getEintragListLenght()-1);

        return this.eintragService.deleteEintrag(userid,eintragid);
    }
}
