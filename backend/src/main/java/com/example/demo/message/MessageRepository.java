package com.example.demo.message;


import com.example.demo.eintrag.Eintrag;
import com.example.demo.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.Stack;


@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {

    List<Message> findAll();

    List<Message> findByUserIdOrderByPostedAtDesc(long id);

    Message findByIdAndUserid(long userid, long messageid);

    Set<Message> findByUserid(long userid);

    Message findByUser(User user);

    Message findMessageById(long id);


















}
