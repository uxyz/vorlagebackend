package com.example.demo.message;


import com.example.demo.UserLike.UserLike;
import com.example.demo.eintrag.Eintrag;
import com.example.demo.eintrag.EintragRepository;
import com.example.demo.ueberschrift.Ueberschrift;
import com.example.demo.user.User;
import com.example.demo.user.UserRepository;
import com.example.demo.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Service
public class MessageService {



    private MessageRepository messageRepository;
    private UserRepository userRepository;
    private UserService userService;

    @Autowired
    public MessageService (MessageRepository messageRepository, UserRepository userRepository, UserService userService
                         ){

        this.messageRepository=messageRepository;
        this.userRepository=userRepository;
        this.userService=userService;
    }


    public Message messageSchreiben(String text, Instant postedAt, long suid, String username, long euid) {


       // Ueberschrift ueberschrift = ueberschriftRepository.findById(id);
        //  ueberschrift.setEintragList(eintragRepository.findEintragByUeberschriftId(id));
        User user = userRepository.findUserById(suid);
        User userx = userRepository.findUserById(euid);
        List<User> userList = new LinkedList<>();
        userList.add(user);
        userList.add(userx);
        String userName= userRepository.findUserById(suid).getUsername();
        String UserNameA = userRepository.findUserById(euid).getUsername();

      //  this.message_userRepository.
      //  this.message_userRepository.



        // user.setUsername(username);
        // ueberschrift.getEintragList().add(eintragRepository.findEintragById(eintragid));




        return messageRepository.save(new Message(userList,text,postedAt,userName,UserNameA,suid,euid));

    }

    public MessageRepository getMessageRepository() {
        return messageRepository;
    }


/*    public Message deletemessage(long messageid) {




        Message  message = this.getMessageRepository().findById(messageid);

        message.setDeletedin(true);

        System.out.println(this.getMessageRepository().findById(messageid).isDeletedin());

        this.getMessageRepository().findById(messageid).setDeletedin(true);

        System.out.println(messageid);
        System.out.println(this.getMessageRepository().findById(messageid).isDeletedin());

            return this.getMessageRepository().findById(messageid);

    }*/
       /* Set<Message> messages = this.messageRepository.findByUserid(messageid);
        for(Message i: messages){
            this.messageRepository.delete(i);

        }

        if (message != null) {

            this.getMessageRepository().delete(message);

        }


         this.messageRepository.findAll().remove(message);

        return this.messageRepository.findAll();

    }*/
}
